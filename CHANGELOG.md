# [2.22.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.21.1...v2.22.0) (2021-05-13)


### Features

* **Charts:** Adding more chart types ([8995608](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/8995608b9a133df246eae4ee85747ce01a21da08))

## [2.21.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.21.0...v2.21.1) (2021-05-13)


### Bug Fixes

* **merge request:** Describe Figma embeds ([d29e286](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/d29e286f4ef6e1931130cc4651830d1b78f8ad1b))

# [2.21.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.20.0...v2.21.0) (2021-05-11)


### Features

* **Markdown:** Add page about text components ([7b45453](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/7b4545313d6f6e1a0357925b38326031b779a421))

# [2.20.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.19.0...v2.20.0) (2021-05-07)


### Features

* **Table:** Add responsive details ([4bc0837](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/4bc0837ca6f4af98c93c5cc8bb3d6f8c82c99999))

# [2.19.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.18.4...v2.19.0) (2021-05-06)


### Features

* **Buttons:** Add hierarchy and variant details ([42d82ac](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/42d82acd1e1c4d235df6d7246dcf1afbcec4c2ae))

## [2.18.4](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.18.3...v2.18.4) (2021-05-04)


### Bug Fixes

* **Iconography:** Add icon meaning details ([973b8ed](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/973b8edc887d4c4a2cb582af78ec5e2dd5b29ff7))

## [2.18.3](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.18.2...v2.18.3) (2021-05-04)


### Bug Fixes

* **charts:** updating chart component guidelines ([e275229](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/e27522960185c43996d01b3660ed7fd708c38256))

## [2.18.2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.18.1...v2.18.2) (2021-04-29)


### Bug Fixes

* **navigation:** Update nav terms to align with tech writing ([ac507ec](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/ac507ec513df4587448653416fd4320b62f354fc))

## [2.18.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.18.0...v2.18.1) (2021-04-21)


### Bug Fixes

* update broken links to button component ([acf8a3f](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/acf8a3f76b7f4dcee00ddf79ce7265e04d9b53a5))

# [2.18.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.17.1...v2.18.0) (2021-04-20)


### Features

* render markdown at build time ([5e78c04](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/5e78c04240faef196765bbe50c19f379132f5f3f))

## [2.17.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.17.0...v2.17.1) (2021-04-15)


### Bug Fixes

* **Table:** Updated Sketch measure link to new Figma component ([f336b3c](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/f336b3c8fb29d4d589d4e58d1ec68a19a084407c))

# [2.17.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.16.0...v2.17.0) (2021-04-09)


### Features

* **GlTooltip:** Add docs around default delay ([6808b1d](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/6808b1d32cc3f84c10a434cb0c269b0ba6ebc972))

# [2.16.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.15.2...v2.16.0) (2021-04-09)


### Features

* **UserHelp:** Clarify usage of help icon ([27c623a](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/27c623a3a2553fa966acf45c240ab8e8c80cd304))

## [2.15.2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.15.1...v2.15.2) (2021-04-05)


### Bug Fixes

* **Spacing:** fix link to gitlab-ui variables ([dbb3411](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/dbb3411b381f78f3c6d9a70df9e7a600ec5bf7fe))

## [2.15.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.15.0...v2.15.1) (2021-04-05)


### Bug Fixes

* Highlight cross-link text as note ([38878cd](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/38878cda0cabb1f7a3c9f5d7102440a91016bee3))

# [2.15.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.14.0...v2.15.0) (2021-04-01)


### Features

* **table:** Update column sorting icons ([b2bc7f5](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/b2bc7f53896ce9aa42051dddd3d42237bc39e968))

# [2.14.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.13.0...v2.14.0) (2021-04-01)


### Features

* **GlBanner:** Separate content sections for banner dismissal options ([34e6d44](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/34e6d44a5a000b68c8ed7277406ad19dd6ec8ab9))

# [2.13.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.12.0...v2.13.0) (2021-03-31)


### Features

* **Color:** Better color palette cross-links ([07e5269](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/07e52690106782d49d87908f1bc759ef0c37726e))

# [2.12.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.11.0...v2.12.0) (2021-03-30)


### Features

* **DataViz:** Clarify system status colors in Pajamas ([ce1fdca](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/ce1fdcaf202639c125138f2cac43eb87906ac161))

# [2.11.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.10.1...v2.11.0) (2021-03-30)


### Features

* **GlButton:** Add guidelines around not grouping danger buttons ([a4992f2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/a4992f21a5549dbf5734e6e378eee280ce3b8d1d))

## [2.10.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.10.0...v2.10.1) (2021-03-26)


### Bug Fixes

* **accessibility:** Use relative links for best practice checklist ([079fbf5](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/079fbf5eefd49c2f651f94aed5955ba23b52e22e))

# [2.10.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.9.0...v2.10.0) (2021-03-26)


### Features

* **accessibility:** Update keyboard only page ([b226080](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/b226080a4f2fcd989a02a633cb3dd7d595142727))

# [2.9.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.8.0...v2.9.0) (2021-03-26)


### Features

* **accessibility:** Add accessibility checklist items to best practices ([a70d9ec](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/a70d9ecd255cbef3106f3d94a51298b68cc3d1bf))

# [2.8.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.7.0...v2.8.0) (2021-03-26)


### Features

* **Glbanner:** Add temp dismissal guidelines ([a4dae84](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/a4dae843673245e7ac538bda74192fbf3881fc42))

# [2.7.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.6.3...v2.7.0) (2021-03-25)


### Features

* **GlPopover:** Update popover docs to specify hover focus ([9a6feaf](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/9a6feafb4978bfdb5efb9907409e1013c1bb65aa))

## [2.6.3](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.6.2...v2.6.3) (2021-03-17)


### Bug Fixes

* **resources:** Removes Dribbble as resource since it is deprecated ([8c347ba](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/8c347ba9f58d8a9971701ee360cdad89a1e12205))

## [2.6.2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.6.1...v2.6.2) (2021-03-16)


### Bug Fixes

* **icons:** Move default pixel grid to top ([c412530](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/c412530f8bd0d01c629096069c9e482b949c9289))

## [2.6.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.6.0...v2.6.1) (2021-03-11)


### Bug Fixes

* **button:** Replace incorrect confirm button image with vue component ([1ab9432](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/1ab9432c3acdd4ada64ac7984ac5118ba4305fad))

# [2.6.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.5.0...v2.6.0) (2021-03-10)


### Features

* **Icons:** Update layout docs ([1597494](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/15974946d273b0c21dffd72b8120d78ad7712b46))

# [2.5.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.4.0...v2.5.0) (2021-03-02)


### Features

* **Button:** Link to button Vue tab ([cbf9d91](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/cbf9d91e5d367536c78b6709b29044d896efec62))

# [2.4.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.3.1...v2.4.0) (2021-02-25)


### Features

* Add documentation about using button labels ([3dcdc6e](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/3dcdc6e9bad9bf377bdcd4be32b499ad49f35c67))

## [2.3.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.3.0...v2.3.1) (2021-02-24)


### Bug Fixes

* **csp:** remove invalid attribute from Content Security Policy ([e5e4d53](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/e5e4d53116bace0afd199f254f3fb34c9c7c2adc))

# [2.3.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.2.1...v2.3.0) (2021-02-23)


### Features

* **Alert:** Add sticky usage docs ([4e88fd7](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/4e88fd7fc51d9334bc970581496683f036de3a55))

## [2.2.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.2.0...v2.2.1) (2021-02-16)


### Bug Fixes

* Update VPAT statement of compliance date and version ([81ccb7e](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/81ccb7e6ee2099f33f21a1b2740df85ee51433e2))

# [2.2.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.1.0...v2.2.0) (2021-02-16)


### Features

* add single stat component page and content ([90a33ac](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/90a33acc30d35bc02c81c0c37d2510421c69979b))

# [2.1.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v2.0.0...v2.1.0) (2021-02-02)


### Features

* **Pagination:** Documenting pagination types ([c738f26](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/c738f260ba84f3771a358ab6d5c7acb8b9bda723))

# [2.0.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.72.0...v2.0.0) (2021-01-28)


### Features

* **GlButton:** Update button variants and add deprecated variants ([0e50563](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/0e505630542404fd4b06a76b8715f9d0552949ba))


### BREAKING CHANGES

* **GlButton:** Updates button variants docs and gitlab-ui examples.
This change deprecates info, success, and warning button variants
and adds the confirm variant.

# [1.72.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.71.0...v1.72.0) (2021-01-21)


### Features

* **voice-tone:** Add active & passive voice example ([e4b555a](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/e4b555ae7827868d35a6b078dd050bd3bde3e0f2))

# [1.71.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.70.1...v1.71.0) (2021-01-20)


### Features

* **GlModal:** Update gitlab-ui status to complete ([ae85c67](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/ae85c679047c22e5fcd6283e6794bfe0603583dc))

## [1.70.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.70.0...v1.70.1) (2021-01-20)


### Bug Fixes

* **Accordion:** update status of docs and GitLab ui ([f16554d](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/f16554deb27cc697d5f8c453db37f4a3b3f91b62))

# [1.70.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.69.0...v1.70.0) (2021-01-08)


### Features

* **spacing:** Add spacing conversion table ([c8fd901](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/c8fd9018ccbf4d3c0cc3da89d9086df05dd77432))

# [1.69.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.68.1...v1.69.0) (2021-01-06)


### Features

* **Alert:** Initial docs update progress ([ab696ca](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/ab696ca4ad6c252ee34995c08c75b083531a7433))

## [1.68.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.68.0...v1.68.1) (2020-12-22)


### Bug Fixes

* avoid loading examples from a similarly named component ([3a0242f](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/3a0242f5ab16dea52afb75a6e84aaa153031623c))

# [1.68.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.67.1...v1.68.0) (2020-12-17)


### Features

* **Accessibility:** Initial addition of accessibility audit guides ([1bff76b](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/1bff76b4890ea082f470d32c725558ae6578fa8c))

## [1.67.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.67.0...v1.67.1) (2020-12-14)


### Bug Fixes

* **naming:** Use singular naming format for consistency ([aba3966](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/aba396657020982104b5d425539f28a9fddab445))

# [1.67.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.66.0...v1.67.0) (2020-12-14)


### Features

* **vpat:** Update VPAT template to use 508 Edition 2.4 ([1fb7b0c](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/1fb7b0ccad24055d604a9e86c46ecb3fa0088a4b))

# [1.66.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.65.0...v1.66.0) (2020-12-14)


### Features

* **spinner:** Remove orange color option ([234fce8](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/234fce87c36ad02ca30a97c90aeccde8501ed3c4))

# [1.65.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.64.0...v1.65.0) (2020-12-11)


### Features

* **buttons:** Correct button categories and simplify docs ([caf155b](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/caf155be71897a267bb45124073082327c30cbd1))

# [1.64.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.63.0...v1.64.0) (2020-12-04)


### Features

* **alert:** Add guidelines around copy in Alert component ([9910416](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/9910416387bfc7258532eb701511a8e702a81a5d))

# [1.63.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.62.0...v1.63.0) (2020-12-04)


### Features

* **Onboarding:** Add established onboarding patterns ([f95c4b5](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/f95c4b507e22439f983e0ace563ae741c445a8e5))

# [1.62.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.61.0...v1.62.0) (2020-12-03)


### Features

* **docs:** Update Pajamas UI Kit with new details ([cbd1783](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/cbd17831e3c72b39552b087163c1efd29e56f64d))

# [1.61.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.60.0...v1.61.0) (2020-12-03)


### Features

* **GlPopover:** Update gitlab ui status to complete ([ef91724](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/ef917248aa089ba0e655222879e601f422bc814a))

# [1.60.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.59.0...v1.60.0) (2020-11-23)


### Features

* **content:** Configuration and settings examples ([e607187](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/e60718719f61ea24998c10bc096a200b4da1bffa))

# [1.59.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.58.0...v1.59.0) (2020-11-19)


### Features

* **merge request:** add MR object docs ([3da5846](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/3da5846b12ab838c273c9daccb5b77c8c1b15d31))

# [1.58.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.57.0...v1.58.0) (2020-11-19)


### Features

* **GlBadge:** Add icon badge example and update font weight guidelines ([d721778](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/d7217789ef11caa8dcac2bd5dfc0d7a12f2a85b4))

# [1.57.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.56.0...v1.57.0) (2020-11-18)


### Features

* mark Card component as built ([4444a75](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/4444a75a2844caca215ff2b55e5382730e68328c))

# [1.56.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.55.0...v1.56.0) (2020-11-16)


### Features

* **Admonitions:** Update admonition styles to be more product-like ([68204e9](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/68204e9ad41cf0e2cdc78c5d16fa1dc499e0b93c))

# [1.55.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.54.2...v1.55.0) (2020-11-13)


### Features

* **GlTooltip:** Update tooltip gitlab-ui status to complete ([abe961d](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/abe961d1c18e97d867af2d13bea9d574716838a8))

## [1.54.2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.54.1...v1.54.2) (2020-11-09)


### Bug Fixes

* Correct anchor to form validations on content page ([47f357e](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/47f357e40f9788a20fa589f0a6b354f6821100ff))

## [1.54.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.54.0...v1.54.1) (2020-11-04)


### Bug Fixes

* **naming:** Use singular naming format for consistency ([04552d7](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/04552d702e45392ab0b2d0661d6262c3fdf8013b))

# [1.54.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.53.0...v1.54.0) (2020-11-03)


### Features

* **content:** Include examples for sign-in and sign in ([0234666](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/0234666371606cb2f03e5065c60b34da3e37c9d5))

# [1.53.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.52.0...v1.53.0) (2020-11-02)


### Features

* **Buttons:** Initial draft for alignment and order ([7788c2e](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/7788c2ec2343db39a3b8d5f237226e9f99d7d9e2))

# [1.52.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.51.0...v1.52.0) (2020-10-30)


### Features

* **Forms:** Add docs for input description text ([a82561a](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/a82561a970e2b2e142971d2590f0db8ff6892b4a))

# [1.51.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.50.1...v1.51.0) (2020-10-29)


### Features

* **GlTabs:** Add justified tabs example in docs ([672b885](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/672b885f3959d98b302af1ffeff59fe87a16a43c))

## [1.50.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.50.0...v1.50.1) (2020-10-15)


### Bug Fixes

* **settings:** Fix path to related components and patterns ([608588b](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/608588be0d9603ef7aab92a387f9cb468565c8a1))

# [1.50.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.49.0...v1.50.0) (2020-10-14)


### Features

* **Forms:** Sync specifications section between checkboxes and radios ([0079a79](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/0079a79105794fef6c38b30e0ef86607b1a93bac))

# [1.49.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.48.0...v1.49.0) (2020-10-14)


### Features

* **settings:** Add new region page for Settings ([b56eb9c](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/b56eb9c05c1d12961ef09a0e7723f7867ae71a8c))

# [1.48.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.47.0...v1.48.0) (2020-10-08)


### Features

* **pagination:** Update pagination with page counters information ([9df1bb7](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/9df1bb7a3cb86257f0c3a89a621c77ee6758113a))

# [1.47.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.46.2...v1.47.0) (2020-10-06)


### Features

* **buttons:** Add info about icon only loading button ([e6642e6](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/e6642e6ac383e4b6ca0317cea15f6cf544e4d2b4))

## [1.46.2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.46.1...v1.46.2) (2020-10-05)


### Bug Fixes

* **deps:** upgrade Nuxt to v2.14.6 ([982106d](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/982106ddb35de917e7785ca24d6387ff254ff3e3))

## [1.46.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.46.0...v1.46.1) (2020-10-02)


### Reverts

* Revert "Update nuxt.config.js" ([83da4b2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/83da4b2f549f2387744a5371b29cdcb7ebb17144))

# [1.46.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.45.0...v1.46.0) (2020-10-02)


### Features

* Add initial gitpod configuration files ([a6ccb40](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/a6ccb40c2672dace3286e86b66b7f200c0a7562f))

# [1.45.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.44.1...v1.45.0) (2020-09-30)


### Features

* **Tabs:** Updating fitted tab guidelines ([4a6eb8f](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/4a6eb8fc398e030df055a9f73e35b25397c88aed))

## [1.44.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.44.0...v1.44.1) (2020-09-30)


### Bug Fixes

* **buttons:** Update guidance on button positioning ([1d17a37](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/1d17a37e7413d4f8f711d504371c5b12f9d3d5b5))

# [1.44.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.43.1...v1.44.0) (2020-09-29)


### Features

* **Forms:** Document different validation types ([db3cf22](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/db3cf2274d15ec20964db0cd837e533ff34eb6f9))

## [1.43.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.43.0...v1.43.1) (2020-09-24)


### Bug Fixes

* **checkbox:** Remove horizontal layout option ([e59aab0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/e59aab0f46608bb6d02401d381cad09d99c3899f))

# [1.43.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.42.0...v1.43.0) (2020-09-21)


### Features

* **GLButton:** Allow for a neutral button to be the primary action ([36b09eb](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/36b09ebecd637386c6f8e7553caef3302690aa7a))

# [1.42.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.41.0...v1.42.0) (2020-09-09)


### Features

* **navigation:** define navigation related terms ([d68b212](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/d68b212867a840fc680346b39998ad46ff865c2f))

# [1.41.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.40.2...v1.41.0) (2020-09-09)


### Features

* **Color:** Update purple 600 hex ([aa628fd](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/aa628fd5877212defc3bbdfbbd5dca14b8ddd2eb))

## [1.40.2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.40.1...v1.40.2) (2020-09-03)


### Bug Fixes

* **skeleton-loader:** Update link to carbon DS loading guidelines ([f7d599a](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/f7d599ab8b1f7247cb510554b418394c985ffa46))

## [1.40.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.40.0...v1.40.1) (2020-09-02)


### Bug Fixes

* Correct link to dropdown specs in Figma ([0438b6f](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/0438b6f0562d47848a27e1c4d65e1c9e8cd8595a))

# [1.40.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.39.1...v1.40.0) (2020-08-27)


### Features

* **contribute:** Add conventional commits to get started ([fd267d3](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/fd267d33efdb2a5333eecb8ef15a5208d00d68c3))

## [1.39.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.39.0...v1.39.1) (2020-08-24)


### Bug Fixes

* **tree:** Update broken link accordionS to accordion ([126b8e3](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/126b8e364c6b05fc540100d0e2623b6077aedf2e))

# [1.39.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.38.0...v1.39.0) (2020-08-20)


### Features

* **alert:** Add guidelines for global alerts ([a772a8e](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/a772a8ea15bb8257c17c35d7dd22e59d5c0b4248))

# [1.38.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.37.1...v1.38.0) (2020-08-19)


### Features

* **layers:** Add layers documentation ([862679f](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/862679f1c5fc77824bfb4da9b8d8759261d67742))

## [1.37.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.37.0...v1.37.1) (2020-08-19)


### Bug Fixes

* **GlDropdown:** Update GlNewDropdown with GlDropdown ([3b6f171](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/3b6f17154c071d14aabd45f003f2b457e0c87cd2))

# [1.37.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.36.0...v1.37.0) (2020-08-18)


### Features

* **Buttons:** Clarify icon use in default dropdown button ([bca9210](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/bca92109066305fa36311409046fea5b395f9503))

# [1.36.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.35.0...v1.36.0) (2020-08-11)


### Features

* **forms:** Add recommended Enter key behavior ([b2aa4a4](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/b2aa4a4c66b06e7e5e19ea761517da846f8851bc))

# [1.35.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.34.0...v1.35.0) (2020-08-04)


### Features

* **content:** Clarify case for features that are also objects ([eb3286a](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/eb3286a24bac073a478b09053b48ac2eba5c05ed))

# [1.34.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.33.0...v1.34.0) (2020-08-03)


### Features

* **alerts:** Document that alerts should be actionable ([393997f](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/393997fd4dba82956e3d751bbedac3d4c6649301))

# [1.33.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.32.0...v1.33.0) (2020-08-03)


### Features

* **Charts:** add popover text wrap info ([fe1443b](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/fe1443b4ffd21f79aae4dd1ca0e71bb8d4a1a84e))

# [1.32.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.31.1...v1.32.0) (2020-07-24)


### Features

* **Buttons:** Including button group example ([087e60a](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/087e60aecdfd59b1177d8f190737a8e3c0433d78))

## [1.31.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.31.0...v1.31.1) (2020-07-23)


### Bug Fixes

* bind aria-expanded to open property ([6a4598f](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/6a4598f9245db00e4b461005aafc9a212abd80ec))

# [1.31.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.30.0...v1.31.0) (2020-07-23)


### Features

* **dropdowns:** Add Figma spec links ([7a66d53](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/7a66d53f1a6ca95715c41c524261f418839c57cc))

# [1.30.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.29.0...v1.30.0) (2020-07-21)


### Features

* **drag-drop:** Add more comprehensive docs for drag drop ([44e7ed7](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/44e7ed7059b50dea2a6cd5da875e47adcfff878e))

# [1.29.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.28.0...v1.29.0) (2020-07-20)


### Features

* **charts:** Add grouped stacked column chart docs ([75c3288](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/75c3288001ccb2fd1f450a79c1b26c6bdf6e0cf5))

# [1.28.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.27.0...v1.28.0) (2020-07-20)


### Features

* **GlModal:** add modal scrollability guidelines ([6334f20](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/6334f20352ab656e389d690754b7802148cdbe0a))

# [1.27.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.26.0...v1.27.0) (2020-07-15)


### Features

* **broadcast:** Add anchor styling specification ([850e88d](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/850e88dcae280cb907a0517aa0e1bc9af812f83a))

# [1.26.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.25.1...v1.26.0) (2020-07-15)


### Features

* **card:** Add details for when and when not to use cards with examples ([e8d23d4](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/e8d23d4df0bc4e61f0ae09c48402cb7fb07f5332))

## [1.25.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.25.0...v1.25.1) (2020-07-09)


### Bug Fixes

* **font-awesome:** Remove font awesome from repo ([26d2e47](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/26d2e47ad8ccc3e391ca24d100a8bb838dc72ec8))

# [1.25.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.24.0...v1.25.0) (2020-07-08)


### Features

* **interaction:** Adds recommendations on external links ([ebd6221](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/ebd62218e1f173b22a9bc2820aacbe5bffaf364a))

# [1.24.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.23.0...v1.24.0) (2020-07-08)


### Features

* **badge:** Add badge demos ([985181a](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/985181acfcb89ada0093c48e4d8cdcbdbce0d2df))
* **dropdowns:** Add dropdown demos ([e3222cf](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/e3222cfa3fad869b0adea62270e68174b9030314))
* **infinite-scroll:** Mark docs complete ([b11915d](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/b11915d20ed66de845c3e5eb3652961b36c048e6))
* **progess-bar:** Add todo back for demo ([d9d04d2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/d9d04d2cbb61342a2455004723395cd158b30d71))
* **progress-bar:** Add demo to progress bar ([869674e](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/869674ee209a59705c89ad865c35e3ac649dc43f))
* **table:** Add demo to tables ([4482eda](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/4482edaf5ac0fb713a64be867287d197a440c6c1))
* **toast:** Add demos to toast ([03a16b3](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/03a16b3d375922cfa29eb41aefccd383d91ddde0))
* **toast:** Change docs status to in progress ([b613071](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/b6130715c06b16ec526ae19752756d97c5ede77d))

# [1.23.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.22.0...v1.23.0) (2020-07-07)


### Features

* **file-uploader:** Add design specs ([e431a67](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/e431a67dda22b0bbafc0992a51fea06e4bd6b203))

# [1.22.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.21.4...v1.22.0) (2020-06-29)


### Features

* **Path:** Add Path component documentation ([a5073af](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/a5073af30722d289a9de3f2b6f76dcf51f7d1687))

## [1.21.4](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.21.3...v1.21.4) (2020-06-26)


### Bug Fixes

* **charts:** Moves charts and data viz components to data viz section ([d3b7cc8](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/d3b7cc80bdf2ac11dc9cecf8767582e53adfad6a))

## [1.21.3](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.21.2...v1.21.3) (2020-06-26)


### Bug Fixes

* Added objects to PJs structure ([ae96b88](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/ae96b88e8ff65d8e1b407d33ac8c397dca828392))

## [1.21.2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.21.1...v1.21.2) (2020-06-25)


### Bug Fixes

* **research:** Correct research links in get started page ([263d29c](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/263d29cecd2a8508f0f6211346c5099bfc591957))

## [1.21.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.21.0...v1.21.1) (2020-06-25)


### Bug Fixes

* **typography:** Remove done todo related to showcasing bold variable ([ca2c3cb](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/ca2c3cb70efb3984cfec4b5c4ead910242ffccfd))

# [1.21.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.20.0...v1.21.0) (2020-06-23)


### Features

* add infinite scroll examples ([abdd198](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/abdd198adec425c6807264c9fc782e9d76737089))

# [1.20.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.19.0...v1.20.0) (2020-06-23)


### Features

* **Color:** Document $purple- color variables ([4599d05](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/4599d05f6714d81627d2095fb6fa298b87626640))

# [1.19.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.18.1...v1.19.0) (2020-06-19)


### Features

* **typography:** Add details about type scales ([ba524c2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/ba524c2c43c0f44f67638d03644b3ccbe8085ebf))

## [1.18.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.18.0...v1.18.1) (2020-06-18)


### Bug Fixes

* **table:** Update & simplify table documentation ([22f1667](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/22f16674e5e89006e88d4175fb09a9feb2c359eb))

# [1.18.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.17.1...v1.18.0) (2020-06-18)


### Features

* **toggles:** Update usage for left aligned labels ([1720a6b](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/1720a6be8010f2b8c5479efeffb0c849f856a4cf))

## [1.17.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.17.0...v1.17.1) (2020-06-16)


### Bug Fixes

* Resolve status.vue compiler issue ([5bc082a](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/5bc082a611b4c89588d4d4ba857a5b919b059789))

# [1.17.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.16.0...v1.17.0) (2020-06-15)


### Features

* **statustable:** Add gitlab ui link to mark toggles complete ([577ca1f](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/577ca1f90e4a30a83dbee9582ec090232a6e6464))

# [1.16.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.15.1...v1.16.0) (2020-06-15)


### Features

* Add hover state to responsive hamburger menu ([8fe57cb](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/8fe57cb0d1595024e2c38e6c3f16c141118bd229))

## [1.15.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.15.0...v1.15.1) (2020-06-12)


### Bug Fixes

* Add max width to page for better readability ([4ea9588](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/4ea95882c34329da5cdd04f8706ce36e5bdfb90f))

# [1.15.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.14.0...v1.15.0) (2020-06-10)


### Features

* **charts:** Add more options menu documentation ([4561b2a](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/4561b2a596583402a9e20a36fbcbbaac48f69b98))

# [1.14.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.13.0...v1.14.0) (2020-06-09)


### Features

* **statustable:** Use gitlab badges for status table ([a1fa460](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/a1fa4605638f30397a8f353b9764229cc9a6e29d))

# [1.13.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.12.2...v1.13.0) (2020-06-09)


### Features

* **tables:** Update table documentation layout ([1ba10f4](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/1ba10f46c70b0328586b90ab8d5539d4a6b38bd6))

## [1.12.2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.12.1...v1.12.2) (2020-06-05)


### Bug Fixes

* **Sorting:** Reformat docs only, no content change ([186a610](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/186a6103b8bf4af5a0741cc091ac2e076b554018))

## [1.12.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.12.0...v1.12.1) (2020-05-28)


### Bug Fixes

* Fix query url for vue ([ac11d9f](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/ac11d9fae1671e5cccf930c6239d27c05d0d9840))

# [1.12.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.11.0...v1.12.0) (2020-05-27)


### Features

* Add param for vue component section ([879f0ea](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/879f0ea7a75a456396118030ea2d91e4c8220e70))

# [1.11.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.10.3...v1.11.0) (2020-05-27)


### Features

* **report:** Add report object documentation ([7293914](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/7293914f01ae4db15d67a420d98d61bdab7e4fcb))

## [1.10.3](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.10.2...v1.10.3) (2020-05-27)


### Bug Fixes

* **file-uploader:** Fix casing to ensure Todo banner is correctly parsed ([e9732b3](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/e9732b3386de7caeb8d001157c3031b921b6d7b6))

## [1.10.2](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.10.1...v1.10.2) (2020-05-25)


### Bug Fixes

* **file-uploader:** Remove broken anchors ([c430214](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/c430214435bbe60bab3579258fb3742202a51fbb))

## [1.10.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.10.0...v1.10.1) (2020-05-22)


### Bug Fixes

* **homepage:** Updated link to Figma UI Kit ([9d3eb22](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/9d3eb224b39a9a0b1aa99282c989727509ea6f95))

# [1.10.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.9.0...v1.10.0) (2020-05-21)


### Features

* **tree:** Add design spec link for Tree component ([0e8def7](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/0e8def726a393f86224623c7b645340df7c99edd))

# [1.9.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.8.0...v1.9.0) (2020-05-20)


### Features

* **Sorting:** Refining sorting vs filtering guidelines ([53f1543](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/53f154311cdef36c08723d5eb40ce5a353e1830e))

# [1.8.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.7.0...v1.8.0) (2020-05-20)


### Features

* **progressbar:** Add documentation for progress bar ([cd2739b](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/cd2739b651cffe2ca6e82ff9e7dafcdef21d2674))

# [1.7.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.6.0...v1.7.0) (2020-05-20)


### Features

* **banner:** Add dismissal guidelines ([1dd8338](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/1dd8338148342f41ecd75fcb900f481e3fc373cf))

# [1.6.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.5.1...v1.6.0) (2020-05-20)


### Features

* **token:** Add Pajamas UI Kit link for tokens ([296ffa1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/296ffa175ca182855d08f8648b9f5f9bf4e63265))

## [1.5.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.5.0...v1.5.1) (2020-05-15)


### Bug Fixes

* **Pajamas UI Kit:** Remove beta reference to correct resource anchor ([da74802](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/da74802b144d5e3f97828ce8df5e1ed8001b2b72))

# [1.5.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.4.0...v1.5.0) (2020-05-14)


### Features

* **fileuploader:** Add documentation for file uploader ([9726a98](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/9726a98dd79629b82c97984f686b50cb33e48193))

# [1.4.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.3.0...v1.4.0) (2020-05-14)


### Features

* **filter:** Add vue component link and component example ([450ac8e](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/450ac8e311ed6198abc175f0ffab6c6770ea1697))

# [1.3.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.2.1...v1.3.0) (2020-05-14)


### Features

* **sorting:** Add Pajamas UI Kit link for sorting component ([b94ef04](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/b94ef04af5f4e03ade50463c353d370a8eef6fbc))

## [1.2.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.2.0...v1.2.1) (2020-05-14)


### Bug Fixes

* **css:** Rename colour gray-0 to gray-10 ([f2376c8](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/f2376c8375f6dee1e87e46eb02e1ca4b06cf97f3))

# [1.2.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.1.1...v1.2.0) (2020-05-13)


### Features

* **statustable:** Add new columns to component status table ([63dc07b](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/63dc07b4d61268f1dc59157471a0bfeeff99daea))

## [1.1.1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.1.0...v1.1.1) (2020-05-12)


### Bug Fixes

* Update design spec links for several components ([d7755a1](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/d7755a1920a65e703027c9236abe64899cbd3c4d))

# [1.1.0](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/compare/v1.0.0...v1.1.0) (2020-05-08)


### Features

* **buttons:** Add position exception for alerts ([f12bb3d](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/f12bb3d555b65c8fa565ffac109cc436ed032322))

# 1.0.0 (2020-04-29)


### Bug Fixes

* Support links to Markdown pages ([8a8e87b](https://gitlab.com/gitlab-org/gitlab-services/design.gitlab.com/commit/8a8e87bf303b481a2dc4eb25b79a3a258868a25f))
